package safecharge

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"pkg.tokend.io/safecharge/internal/resources"
)

var ErrNoPaymentMethod = errors.New("no payment method")

type Connector struct {
	url               string
	merchantID        string
	merchantSiteID    string
	merchantSecretKey string
	client            *http.Client
}

func NewConnector(url, merchantID, merchantSiteID, merchantSecretKey string) *Connector {
	return &Connector{
		url:               url,
		merchantID:        merchantID,
		merchantSiteID:    merchantSiteID,
		merchantSecretKey: merchantSecretKey,
		client:            http.DefaultClient,
	}
}

const (
	StatusError    = "ERROR"
	StatusSuccess  = "SUCCESS"
	StatusApproved = "APPROVED"
	StatusDeclined = "DECLINED"
)

type PaymentOpts struct {
	SessionToken    string
	ClientRequestID string
	Currency        string
	Amount          string
	TempToken       string
	CardHolderName  string
	CVV             string
	Country         string
	Email           string
	DeviceType      string
	Browser         string
	IPAddress       string
}

type ChargeOpts struct {
	SessionToken    string
	UserTokenID     string
	ClientRequestID string
	Amount          string
	Currency        string
	DeviceType      string
	Browser         string
	IPAddress       string
}

func (c *Connector) PerformCharge(opts ChargeOpts) error {
	payment := c.createPayment(opts.DeviceType, opts.Browser, opts.IPAddress, opts.Amount)
	payment.UserTokenID = opts.UserTokenID
	payment.ClientRequestID = opts.ClientRequestID
	payment.SessionToken = opts.SessionToken
	payment.Currency = opts.Currency

	paymentMethod, err := c.getCreditCardUPO(opts.ClientRequestID, opts.UserTokenID)
	if err != nil {
		return errors.Wrap(err, "failed to get payment method")
	}

	if paymentMethod == nil {
		return ErrNoPaymentMethod
	}

	payment.BillingAddress = resources.BillingAddress{
		Country: paymentMethod.BillingAddress.Country,
		Email:   paymentMethod.BillingAddress.Email,
	}
	payment.UserPaymentOption = &resources.UserPaymentOption{
		UserPaymentOptionID: paymentMethod.UserPaymentOptionID,
	}

	err = c.performPayment(payment)
	if err != nil {
		return errors.Wrap(err, "failed to perform payment")
	}
	return nil
}

func (c *Connector) createPayment(deviceType, browser, ipAddress, amount string) resources.Payment {
	if browser == "" {
		browser = "browser"
	}
	if deviceType == "" {
		deviceType = "DESKTOP"
	}
	if ipAddress == "" {
		ipAddress = "0.0.0.0"
	}

	deviceDetails := resources.DeviceDetails{
		DeviceType: deviceType,
		Browser:    browser,
		IPAddress:  ipAddress,
	}

	amountDetails := resources.AmountDetails{
		TotalShipping: "0",
		TotalHandling: "0",
		TotalDiscount: "0",
		TotalTax:      "0",
	}

	items := []resources.Item{
		resources.Item{Name: "name", Quantity: "1", Price: amount},
	}

	return resources.Payment{
		MerchantID:        c.merchantID,
		MerchantSiteID:    c.merchantSiteID,
		TransactionType:   "Sale",
		IsRebilling:       "0",
		IsPartialApproval: "0",
		Amount:            amount,
		AmountDetails:     amountDetails,
		Items:             items,
		DeviceDetails:     deviceDetails,
	}
}

// PerformPayment performing credit card payments, using a card temporary token
func (c *Connector) PerformPayment(opts PaymentOpts) error {
	payment := c.createPayment(opts.DeviceType, opts.Browser, opts.IPAddress, opts.Amount)
	payment.BillingAddress = resources.BillingAddress{
		Country: opts.Country,
		Email:   opts.Email,
	}
	payment.ClientRequestID = opts.ClientRequestID
	payment.ClientUniqueID = opts.ClientRequestID
	payment.Currency = opts.Currency
	payment.SessionToken = opts.SessionToken
	payment.CardData = &resources.CardData{
		CardHolderName: opts.CardHolderName,
		TempToken:      opts.TempToken,
		CVV:            opts.CVV,
	}
	err := c.performPayment(payment)
	if err != nil {
		return errors.Wrap(err, "payment failed")
	}

	return nil
}

// GetSessionToken returns unique token that represents the client session.
// it is required to use the same sessionToken value from its creation and by the end of the payment flow
func (c *Connector) GetSessionToken(clientRequestID string) (string, error) {
	url := fmt.Sprintf("%s/v1/getSessionToken.do", c.url)
	body, err := c.newSessionTokenRequest(clientRequestID)
	if err != nil {
		return "", errors.Wrap(err, "failed to create session token request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return "", errors.Wrap(err, "failed to get session token")
	}

	var result resources.SessionTokenResult

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return "", errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusSuccess:
		return result.SessionToken, nil
	case StatusError:
		return "", errors.From(errors.New("failed to get session token"), logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}

// newSessionTokenRequest creates request for getting session token
func (c *Connector) newSessionTokenRequest(clientRequestID string) (io.Reader, error) {
	ts := c.getTimestamp()
	checksum := c.getChecksum(c.merchantID, c.merchantSiteID, clientRequestID, ts, c.merchantSecretKey)

	request := resources.SessionTokenRequest{
		MerchantID:      c.merchantID,
		MerchantSiteID:  c.merchantSiteID,
		ClientRequestID: clientRequestID,
		TimeStamp:       ts,
		Checksum:        checksum,
	}
	req, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal session token request")
	}
	return bytes.NewBuffer(req), nil
}

// getChecksum return hashed values of the request parameters, which are concatenated in the order in which they were transferred
func (c *Connector) getChecksum(values ...string) string {
	var result []string
	for _, v := range values {
		result = append(result, v)
	}
	value := strings.Join(result, "")

	h := sha256.New()
	_, err := h.Write([]byte(value))
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}

// getTimestamp returns time when the method call is performed in the format: YYYYMMDDHHmmss
func (c *Connector) getTimestamp() string {
	return time.Now().Format("20060102150405")
}
