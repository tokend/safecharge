package safecharge

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

var Errors = map[int]error{
	1061: validation.Errors{"card token": ErrInvalidProvidedData},
	1062: validation.Errors{"cvv": ErrInvalidProvidedData},
	1064: validation.Errors{"session token": ErrInvalidProvidedData},
	1065: validation.Errors{"token": ErrInvalidProvidedData},
	1019: validation.Errors{"card token or user token": errors.New("token size must be between 1 and 45")},
	1010: validation.Errors{"user token": ErrInvalidProvidedData},
	1011: validation.Errors{"payment option": errors.New("payment option is not found or it doesn't belong to the user")},
}
