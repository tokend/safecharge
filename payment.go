package safecharge

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"pkg.tokend.io/safecharge/internal/resources"
)

var (
	ErrInvalidProvidedData = errors.New("invalid provided data")
	ErrPaymentRequest      = errors.New("payment request failed")
)

func (c *Connector) performPayment(payment resources.Payment) error {
	url := fmt.Sprintf("%s/v1/paymentCC.do", c.url)

	body, err := c.newPaymentRequest(payment)
	if err != nil {
		return errors.Wrap(err, "failed to create payment request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return errors.Wrap(err, "failed to create payment")
	}

	var result resources.PaymentResult

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusError:
		err, ok := Errors[result.ErrCode]
		if ok {
			return err
		}
		return errors.From(
			ErrPaymentRequest,
			logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	case StatusSuccess:
		switch result.TransactionStatus {
		case StatusApproved:
			return nil
		case StatusDeclined, StatusError:
			return errors.From(errors.New("transaction failed"),
				logan.F{
					"transaction_status": result.TransactionStatus,
					"gw_error_code":      result.ErrCode,
					"gw_error_reason":    result.Reason})
		default:
			panic(fmt.Sprintf("unknown transaction_status: %s", result.TransactionStatus))
		}
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}

func (c *Connector) newPaymentRequest(payment resources.Payment) (io.Reader, error) {
	ts := c.getTimestamp()
	payment.TimeStamp = ts

	payment.Checksum = c.getChecksum(
		c.merchantID,
		c.merchantSiteID,
		payment.ClientRequestID,
		payment.Amount,
		payment.Currency,
		payment.TimeStamp,
		c.merchantSecretKey)

	req, err := json.Marshal(payment)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal payment request")
	}
	return bytes.NewBuffer(req), nil
}
