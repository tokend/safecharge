package safecharger // import "pkg.tokend.io/safecharge/safecharger"

import (
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"pkg.tokend.io/safecharge"
)

type Safecharger interface {
	Safecharge() *safecharge.Connector
}

type safecharger struct {
	getter kv.Getter
	*comfig.Once
}

func New(getter kv.Getter) Safecharger {
	return &safecharger{
		getter: getter,
		Once:   &comfig.Once{},
	}
}

func (s safecharger) Safecharge() *safecharge.Connector {
	return s.Do(func() interface{} {
		var config struct {
			URL               string `fig:"url,required"`
			MerchantID        string `fig:"merchant_id,required"`
			MerchantSiteID    string `fig:"merchant_site_id,required"`
			MerchantSecretKey string `fig:"merchant_secret_key,required"`
		}
		err := figure.
			Out(&config).
			From(kv.MustGetStringMap(s.getter, "safecharge")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out safecharge"))
		}
		return safecharge.NewConnector(
			config.URL, config.MerchantID,
			config.MerchantSiteID, config.MerchantSecretKey,
		)
	}).(*safecharge.Connector)
}
