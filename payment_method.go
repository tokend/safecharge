package safecharge

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strconv"

	"gitlab.com/distributed_lab/logan/v3"

	"pkg.tokend.io/safecharge/internal/resources"

	"gitlab.com/distributed_lab/logan/v3/errors"
)

type PaymentMethodOpts struct {
	SessionToken    string
	UserTokenID     string
	ClientRequestID string
	CardToken       string
	Country         string
	Email           string
	Address         *string
	City            *string
	County          *string
	FirstName       *string
	LastName        *string
	Phone           *string
	State           *string
	Zip             *string
}

func (c *Connector) AddUPO(opts PaymentMethodOpts) error {
	userExist, err := c.UserExist(opts.UserTokenID, opts.ClientRequestID)
	if err != nil {
		return errors.Wrap(err, "failed to get user")
	}

	if !userExist {
		err := c.createUser(opts.ClientRequestID, opts.UserTokenID, opts.Country)
		if err != nil {
			return errors.Wrap(err, "failed to create user")
		}
	}

	url := fmt.Sprintf("%s/v1/addUPOCreditCardByTempToken.do", c.url)
	body, err := c.newUPORequest(opts)
	if err != nil {
		return errors.Wrap(err, "failed to create payment request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return errors.Wrap(err, "failed to create payment method")
	}

	var result resources.Result

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusSuccess:
		return nil
	case StatusError:
		err, ok := Errors[result.ErrCode]
		if ok {
			return err
		}
		return errors.From(errors.New("failed to create payment option with temporary token"), logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}

func (c *Connector) newUPORequest(opts PaymentMethodOpts) (io.Reader, error) {
	ts := c.getTimestamp()
	checksum := c.getChecksum(c.merchantID, c.merchantSiteID, opts.ClientRequestID, ts, c.merchantSecretKey)

	request := resources.CreatePaymentMethodRequest{
		SessionToken:    opts.SessionToken,
		MerchantID:      c.merchantID,
		MerchantSiteID:  c.merchantSiteID,
		UserTokenID:     opts.UserTokenID,
		ClientRequestID: opts.ClientRequestID,
		CcTempToken:     opts.CardToken,
		BillingAddress: resources.BillingAddress{
			Email:     opts.Email,
			Country:   opts.Country,
			City:      opts.City,
			County:    opts.County,
			Address:   opts.Address,
			Zip:       opts.Zip,
			Phone:     opts.Phone,
			FirstName: opts.FirstName,
			LastName:  opts.LastName,
			State:     opts.State,
		},
		TimeStamp: ts,
		Checksum:  checksum,
	}
	req, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal payment option request")
	}
	return bytes.NewBuffer(req), nil
}

func (c *Connector) ListUPOs(address, clientRequestID string) ([]PaymentMethod, error) {
	url := fmt.Sprintf("%s/v1/getUserUPOs.do", c.url)
	body, err := c.newUserUPOsRequest(clientRequestID, address)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create user payment options request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user payment options")
	}

	var result PaymentMethodsResult
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusSuccess:
		return result.PaymentMethods, nil
	case StatusError:
		err, ok := Errors[result.ErrCode]
		if ok {
			return nil, err
		}
		return nil, errors.From(errors.New("failed to get payment method"), logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}

func (c *Connector) newUserUPOsRequest(clientRequestID, userTokenID string) (io.Reader, error) {
	ts := c.getTimestamp()
	checksum := c.getChecksum(c.merchantID, c.merchantSiteID, userTokenID, clientRequestID, ts, c.merchantSecretKey)

	request := resources.ListPaymentMethodsRequest{
		MerchantID:      c.merchantID,
		MerchantSiteID:  c.merchantSiteID,
		UserTokenID:     userTokenID,
		ClientRequestID: clientRequestID,
		TimeStamp:       ts,
		Checksum:        checksum,
	}
	req, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal user payment options request")
	}
	return bytes.NewBuffer(req), nil
}

func (c *Connector) DeleteUPO(address, clientRequestID string, paymentOptionID int) error {
	url := fmt.Sprintf("%s/v1/deleteUPO.do", c.url)
	body, err := c.newDeleteUPORequest(clientRequestID, address, paymentOptionID)
	if err != nil {
		return errors.Wrap(err, "failed to create request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return errors.Wrap(err, "failed to delete user payment option")
	}
	var result resources.Result

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusSuccess:
		return nil
	case StatusError:
		err, ok := Errors[result.ErrCode]
		if ok {
			return err
		}
		return errors.From(errors.New("failed to delete payment method"), logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}

func (c *Connector) newDeleteUPORequest(clientRequestID, userTokenID string, paymentOptionID int) (io.Reader, error) {
	ts := c.getTimestamp()
	checksum := c.getChecksum(c.merchantID, c.merchantSiteID, userTokenID, clientRequestID, strconv.Itoa(paymentOptionID), ts, c.merchantSecretKey)

	request := resources.DeletePaymentMethodRequest{
		MerchantID:          c.merchantID,
		MerchantSiteID:      c.merchantSiteID,
		UserTokenID:         userTokenID,
		ClientRequestID:     clientRequestID,
		UserPaymentOptionID: paymentOptionID,
		TimeStamp:           ts,
		Checksum:            checksum,
	}

	req, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal delete payment option request")
	}
	return bytes.NewBuffer(req), nil
}

func (c *Connector) HasActivePaymentMethod(address, clientRequestID string) (bool, error) {
	paymentMethod, err := c.getCreditCardUPO(clientRequestID, address)
	if err != nil {
		return false, errors.Wrap(err, "failed to get payment option")
	}

	if paymentMethod == nil {
		return false, nil
	}

	if paymentMethod.UpoStatus != "enabled" {
		return false, nil
	}
	//FIXME check payment expiryDate instead?

	return true, nil
}

// getCreditCardUPO returns payment method. Returns nil,nil if method doesn't exist
func (c *Connector) getCreditCardUPO(clientRequestID, userTokenID string) (*PaymentMethod, error) {
	url := fmt.Sprintf("%s/v1/getUserUPOs.do", c.url)
	body, err := c.newUserUPOsRequest(clientRequestID, userTokenID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create user payment options request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user payment options")
	}

	var result PaymentMethodsResult
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusSuccess:
		for _, method := range result.PaymentMethods {
			if method.PaymentMethodName == "cc_card" {
				return &method, nil
			}
		}
		return nil, ErrNoPaymentMethod
	case StatusError:
		return nil, errors.From(errors.New("failed to get payment method"), logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}

type PaymentMethodsResult struct {
	Status         string          `json:"status"`
	ErrCode        int             `json:"errCode"`
	Reason         string          `json:"reason"`
	Version        string          `json:"version"`
	PaymentMethods []PaymentMethod `json:"paymentMethods"`
}

type PaymentMethod struct {
	UserPaymentOptionID int            `json:"userPaymentOptionId"`
	PaymentMethodName   string         `json:"paymentMethodName"`
	UpoName             string         `json:"upoName"`
	UpoRegistrationDate string         `json:"upoRegistrationDate"`
	ExpiryDate          string         `json:"expiryDate"`
	BillingAddress      BillingAddress `json:"billingAddress"`
	UpoData             UpoData        `json:"upoData"`
	UpoStatus           string         `json:"upoStatus"`
}

type UpoData struct {
	CardType         string `json:"cardType"`
	CcCardNumber     string `json:"ccCardNumber"`
	CcCardNumberHash string `json:"ccCardNumberHash"`
	CcExpMonth       string `json:"ccExpMonth"`
	CcExpYear        string `json:"ccExpYear"`
	CcNameOnCard     string `json:"ccNameOnCard"`
	CcToken          string `json:"ccToken"`
	Brand            string `json:"brand"`
	UniqueCC         string `json:"uniqueCC"`
	Bin              string `json:"bin"`
	Last4Digits      string `json:"last4Digits"`
}

type BillingAddress struct {
	Country   string  `json:"countryCode"`
	Email     string  `json:"email"`
	Address   *string `json:"address,omitempty"`
	City      *string `json:"city,omitempty"`
	County    *string `json:"county,omitempty"`
	FirstName *string `json:"firstName,omitempty"`
	LastName  *string `json:"lastName,omitempty"`
	Phone     *string `json:"phone,omitempty"`
	State     *string `json:"state,omitempty"`
	Zip       *string `json:"zip,omitempty"`
}
