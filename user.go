package safecharge

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/distributed_lab/logan/v3"

	"pkg.tokend.io/safecharge/internal/resources"

	"gitlab.com/distributed_lab/logan/v3/errors"
)

const UserNotFound = "User with the specified token is not found!"

func (c *Connector) createUser(clientRequestID, userTokenID, countryCode string) error {
	url := fmt.Sprintf("%s/v1/createUser.do", c.url)
	body, err := c.newCreateUserRequest(clientRequestID, userTokenID, countryCode)
	if err != nil {
		return errors.Wrap(err, "failed to create user creation request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return errors.Wrap(err, "failed to create user")
	}

	var result resources.Result

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusSuccess:
		return nil
	case StatusError:
		return errors.From(errors.New("failed to create user"), logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}

func (c *Connector) newCreateUserRequest(clientRequestID, userTokenID, countryCode string) (io.Reader, error) {
	ts := c.getTimestamp()

	request := resources.CreateUserRequest{
		MerchantID:      c.merchantID,
		MerchantSiteID:  c.merchantSiteID,
		UserTokenID:     userTokenID,
		ClientRequestID: clientRequestID,
		CountryCode:     countryCode,
		TimeStamp:       ts,
	}

	checksum := c.getChecksum(c.merchantID, c.merchantSiteID, userTokenID, clientRequestID, countryCode, ts, c.merchantSecretKey)
	request.Checksum = checksum
	req, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal user creation request")
	}
	return bytes.NewBuffer(req), nil
}

func (c *Connector) newUserDetailsRequest(clientRequestID, userTokenID string) (io.Reader, error) {
	ts := c.getTimestamp()
	checksum := c.getChecksum(c.merchantID, c.merchantSiteID, userTokenID, clientRequestID, ts, c.merchantSecretKey)

	request := resources.UserDetailsRequest{
		MerchantID:      c.merchantID,
		MerchantSiteID:  c.merchantSiteID,
		UserTokenID:     userTokenID,
		ClientRequestID: clientRequestID,
		TimeStamp:       ts,
		Checksum:        checksum,
	}

	req, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal user details request")
	}
	return bytes.NewBuffer(req), nil
}
func (c *Connector) UserExist(address, clientRequestID string) (bool, error) {
	url := fmt.Sprintf("%s/v1/getUserDetails.do", c.url)
	body, err := c.newUserDetailsRequest(clientRequestID, address)
	if err != nil {
		return false, errors.Wrap(err, "failed to create user details request")
	}

	resp, err := c.client.Post(url, "application/json", body)
	if err != nil {
		return false, errors.Wrap(err, "failed to get user details")
	}

	var result resources.Result
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return false, errors.Wrap(err, "failed to decode response body")
	}

	switch result.Status {
	case StatusSuccess:
		return true, nil
	case StatusError:
		if result.Reason == UserNotFound {
			return false, nil
		}
		return false, errors.From(errors.New("failed to get"), logan.F{"error_code": result.ErrCode, "reason": result.Reason})
	default:
		panic(fmt.Sprintf("unknown result status: %s", result.Status))
	}
}
