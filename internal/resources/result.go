package resources

type Result struct {
	Status  string `json:"status"`
	ErrCode int    `json:"errCode"`
	Reason  string `json:"reason"`
}
