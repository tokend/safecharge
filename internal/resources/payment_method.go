package resources

type CreatePaymentMethodRequest struct {
	SessionToken    string         `json:"sessionToken"`
	MerchantID      string         `json:"merchantId"`
	MerchantSiteID  string         `json:"merchantSiteId"`
	UserTokenID     string         `json:"userTokenId"`
	ClientRequestID string         `json:"clientRequestId"`
	CcTempToken     string         `json:"ccTempToken"`
	BillingAddress  BillingAddress `json:"billingAddress"`
	TimeStamp       string         `json:"timeStamp"`
	Checksum        string         `json:"checksum"`
}

type ListPaymentMethodsRequest struct {
	MerchantID      string `json:"merchantId"`
	MerchantSiteID  string `json:"merchantSiteId"`
	UserTokenID     string `json:"userTokenId"`
	ClientRequestID string `json:"clientRequestId"`
	TimeStamp       string `json:"timeStamp"`
	Checksum        string `json:"checksum"`
}

type DeletePaymentMethodRequest struct {
	MerchantID          string `json:"merchantId"`
	MerchantSiteID      string `json:"merchantSiteId"`
	UserTokenID         string `json:"userTokenId"`
	ClientRequestID     string `json:"clientRequestId"`
	UserPaymentOptionID int    `json:"userPaymentOptionId"`
	TimeStamp           string `json:"timeStamp"`
	Checksum            string `json:"checksum"`
}
