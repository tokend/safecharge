package resources

type CreateUserRequest struct {
	MerchantID      string `json:"merchantId"`
	MerchantSiteID  string `json:"merchantSiteId"`
	UserTokenID     string `json:"userTokenId"`
	ClientRequestID string `json:"clientRequestId"`
	CountryCode     string `json:"countryCode"`
	TimeStamp       string `json:"timeStamp"`
	Checksum        string `json:"checksum"`
}

type UserDetailsRequest struct {
	Environment     string `json:"environment"`
	MerchantID      string `json:"merchantId"`
	MerchantSiteID  string `json:"merchantSiteId"`
	UserTokenID     string `json:"userTokenId"`
	ClientRequestID string `json:"clientRequestId"`
	TimeStamp       string `json:"timeStamp"`
	Checksum        string `json:"checksum"`
}
