package resources

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSessionTokenResult(t *testing.T) {
	t.Run("valid", func(t *testing.T) {
		response := []byte(`{"sessionToken":"89aa73eb-0364-453d-829e-e3d5e6c2676d","internalRequestId":132991433,"status":"SUCCESS","errCode":0,"reason":"","version":"1.0","clientRequestId":"test"}`)
		expected := SessionTokenResult{
			Status: "SUCCESS",
		}

		got := SessionTokenResult{}
		err := json.NewDecoder(bytes.NewBuffer(response)).Decode(&got)
		assert.NoError(t, err)
		assert.Equal(t, expected.Status, got.Status)
		assert.Equal(t, expected.ErrCode, got.ErrCode)
		assert.Equal(t, expected.Reason, got.Reason)
	})

	t.Run("invalid id", func(t *testing.T) {
		response := []byte(`{"sessionToken":"","internalRequestId":132991903,"status":"ERROR","errCode":1013,"reason":"Invalid merchant Id.","version":"1.0","clientRequestId":"test"}`)
		expected := SessionTokenResult{
			Status:  "ERROR",
			ErrCode: 1013,
			Reason:  "Invalid merchant Id.",
		}

		got := SessionTokenResult{}
		err := json.NewDecoder(bytes.NewBuffer(response)).Decode(&got)
		assert.NoError(t, err)
		assert.Equal(t, expected.Status, got.Status)
		assert.Equal(t, expected.ErrCode, got.ErrCode)
		assert.Equal(t, expected.Reason, got.Reason)
	})
}
