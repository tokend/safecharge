package resources

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPaymentResult(t *testing.T) {
	t.Run("approved", func(t *testing.T) {
		response := []byte(`{"orderId":240947933,"transactionStatus":"APPROVED","gwErrorCode":0,"gwExtendedErrorCode":0,"userPaymentOptionId":"","externalTransactionId":"","transactionId":"1110000000000324293","authCode":"111542","userTokenId":"","CVV2Reply":"","AVSCode":"","customData":"","transactionType":"Sale","partialApprovalDetails":{"isPartialApproval":"0","amountInfo":{"requestedAmount":"16","requestedCurrency":"USD","processedAmount":"16","processedCurrency":"USD"}},"fraudDetails":{"finalDecision":"Accept"},"acquirerId":"19","bin":"400002","last4Digits":"0961","ccCardNumber":"4****0961","ccExpMonth":"01","ccExpYear":"20","sessionToken":"1a4a1e72-eedf-40ad-a550-75ead83fbce9","clientUniqueId":"JEDYFCUHUQPJGSCI6RCBGHJED","internalRequestId":132925103,"status":"SUCCESS","errCode":0,"reason":"","version":"1.0","clientRequestId":"JEDYFCUHUQPJGSCI6RCBGHJED"}`)
		expected := PaymentResult{
			Status:            "SUCCESS",
			TransactionStatus: "APPROVED",
		}

		got := PaymentResult{}
		err := json.NewDecoder(bytes.NewBuffer(response)).Decode(&got)
		assert.NoError(t, err)
		assert.Equal(t, expected.Status, got.Status)
		assert.Equal(t, expected.TransactionStatus, got.TransactionStatus)
		assert.Equal(t, expected.ErrCode, got.ErrCode)
		assert.Equal(t, expected.Reason, got.Reason)
		assert.Equal(t, expected.GwErrorCode, got.GwErrorCode)
		assert.Equal(t, expected.GwErrorReason, got.GwErrorReason)
	})

	t.Run("error status", func(t *testing.T) {
		response := []byte(`{"userPaymentOptionId":"","userTokenId":"","sessionToken":"178b3655-745d-4338-977b-01cf96f80d23","clientUniqueId":"XLVIZGSHSN3OUAPWJ5MJEZMJOE","internalRequestId":132924143,"status":"ERROR","errCode":1019,"reason":"cardData.CVV is invalid","version":"1.0","clientRequestId":"XLVIZGSHSN3OUAPWJ5MJEZMJOE"}`)
		expected := PaymentResult{
			Status:  "ERROR",
			ErrCode: 1019,
			Reason:  "cardData.CVV is invalid",
		}

		got := PaymentResult{}
		err := json.NewDecoder(bytes.NewBuffer(response)).Decode(&got)
		assert.NoError(t, err)
		assert.Equal(t, expected.Status, got.Status)
		assert.Equal(t, expected.TransactionStatus, got.TransactionStatus)
		assert.Equal(t, expected.ErrCode, got.ErrCode)
		assert.Equal(t, expected.Reason, got.Reason)
		assert.Equal(t, expected.GwErrorCode, got.GwErrorCode)
		assert.Equal(t, expected.GwErrorReason, got.GwErrorReason)
	})

	t.Run("transaction error", func(t *testing.T) {
		response := []byte(`{"orderId":240949223,"transactionStatus":"ERROR","gwErrorCode":-1100,"gwErrorReason":"3D Related transaction is missing or incorrect","gwExtendedErrorCode":1155,"userPaymentOptionId":"","externalTransactionId":"","transactionId":"1110000000000324491","authCode":"","userTokenId":"","CVV2Reply":"","AVSCode":"","customData":"","transactionType":"Sale","partialApprovalDetails":{"isPartialApproval":"0","amountInfo":{"requestedAmount":"160","requestedCurrency":"USD","processedAmount":"160","processedCurrency":"USD"}},"acquirerId":"19","bin":"400002","last4Digits":"0961","ccCardNumber":"4****0961","ccExpMonth":"01","ccExpYear":"20","sessionToken":"d02d4031-2676-471b-953b-dd21375621ff","clientUniqueId":"QE3NZ5LGY6ZRKKZ5NJZYWDWAXU","internalRequestId":132928833,"status":"SUCCESS","errCode":0,"reason":"","version":"1.0","clientRequestId":"QE3NZ5LGY6ZRKKZ5NJZYWDWAXU"}`)

		expected := PaymentResult{
			Status:            "SUCCESS",
			ErrCode:           0,
			Reason:            "",
			TransactionStatus: "ERROR",
			GwErrorCode:       -1100,
			GwErrorReason:     "3D Related transaction is missing or incorrect",
		}

		got := PaymentResult{}
		err := json.NewDecoder(bytes.NewBuffer(response)).Decode(&got)
		assert.NoError(t, err)
		assert.Equal(t, expected.Status, got.Status)
		assert.Equal(t, expected.TransactionStatus, got.TransactionStatus)
		assert.Equal(t, expected.ErrCode, got.ErrCode)
		assert.Equal(t, expected.Reason, got.Reason)
		assert.Equal(t, expected.GwErrorCode, got.GwErrorCode)
		assert.Equal(t, expected.GwErrorReason, got.GwErrorReason)
	})
}
