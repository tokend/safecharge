package resources

type CardData struct {
	TempToken      string `json:"ccTempToken"`
	CardHolderName string `json:"cardHolderName"`
	CVV            string `json:"CVV"`
}

// country parameter is always mandatory
type BillingAddress struct {
	Country   string  `json:"country"`
	Email     string  `json:"email"`
	Address   *string `json:"address,omitempty"`
	City      *string `json:"city,omitempty"`
	County    *string `json:"county,omitempty"`
	FirstName *string `json:"firstName,omitempty"`
	LastName  *string `json:"lastName,omitempty"`
	Phone     *string `json:"phone,omitempty"`
	State     *string `json:"state,omitempty"`
	Zip       *string `json:"zip,omitempty"`
}

type Payment struct {
	SessionToken   string `json:"sessionToken"`
	MerchantID     string `json:"merchantId"`
	MerchantSiteID string `json:"merchantSiteId"`
	// ID of the user in the merchant’s system
	UserTokenID string `json:"userTokenId"`
	// ID of the transaction in the merchant’s system
	ClientUniqueID string `json:"clientUniqueId"`
	// ID of the API request in the merchant’s system
	ClientRequestID   string             `json:"clientRequestId"`
	TransactionType   string             `json:"transactionType"`
	IsRebilling       string             `json:"isRebilling"`
	IsPartialApproval string             `json:"isPartialApproval"`
	Currency          string             `json:"currency"`
	Amount            string             `json:"amount"`
	AmountDetails     AmountDetails      `json:"amountDetails"`
	Items             []Item             `json:"items"`
	DeviceDetails     DeviceDetails      `json:"deviceDetails"`
	CardData          *CardData          `json:"cardData"`
	UserPaymentOption *UserPaymentOption `json:"userPaymentOption"`
	BillingAddress    BillingAddress     `json:"billingAddress"`
	TimeStamp         string             `json:"timeStamp"`
	Checksum          string             `json:"checksum"`
}

type UserPaymentOption struct {
	UserPaymentOptionID int `json:"userPaymentOptionId"`
}

type Item struct {
	Name     string `json:"name"`
	Price    string `json:"price"`
	Quantity string `json:"quantity"`
}

type AmountDetails struct {
	TotalShipping string `json:"totalShipping"`
	TotalHandling string `json:"totalHandling"`
	TotalDiscount string `json:"totalDiscount"`
	TotalTax      string `json:"totalTax"`
}

type DeviceDetails struct {
	DeviceType string `json:"deviceType"`
	DeviceName string `json:"deviceName"`
	DeviceOS   string `json:"deviceOS"`
	Browser    string `json:"browser"`
	IPAddress  string `json:"ipAddress"`
}

type PaymentResult struct {
	Status            string `json:"status"`
	TransactionStatus string `json:"transactionStatus"`
	ErrCode           int    `json:"errCode"`
	Reason            string `json:"reason"`
	GwErrorCode       int    `json:"gwErrorCode"`
	GwErrorReason     string `json:"gwErrorReason"`
}
