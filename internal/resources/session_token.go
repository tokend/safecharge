package resources

type SessionTokenRequest struct {
	MerchantID      string `json:"merchantId"`
	MerchantSiteID  string `json:"merchantSiteId"`
	ClientRequestID string `json:"clientRequestId"`
	TimeStamp       string `json:"timeStamp"`
	Checksum        string `json:"checksum"`
}

type SessionTokenResult struct {
	SessionToken string `json:"sessionToken"`
	Status       string `json:"status"`
	ErrCode      int    `json:"errCode"`
	Reason       string `json:"reason"`
}
